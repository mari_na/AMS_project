package com.example.demo.repository.category;

import java.util.List;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Category.Category;
@Repository
public interface CategoryRepository {
	
	public void add(Category category);
	
	@Select("SELECT * FROM tb_category WHERE id=#{id}")
	public  Category findOne(int id);
	
	@Select("SELECT * FROM tb_category")
	public  List<Category> findAll();
}
