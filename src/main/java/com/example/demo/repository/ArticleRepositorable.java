package com.example.demo.repository;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.stereotype.Repository;

import com.example.demo.model.Article;
@Repository
@MapperScan("com.example.demo.repository")
public interface ArticleRepositorable {
	@Insert("INSERT INTO tb_articles(title,author,thumbnail,created_date,category_id) VALUES(#{name},#{author},#{thumnailURL},#{date}),#{Category.id}")
	public void add(Article article);
	
	@Select("SELECT id,title,author,thumbnail,a.created_date,c.category_id, c.name as category_name "
			+"from tb_articles a INNER JOIN tb_category c"
			+ " ON  a.category_id=c.id WHERE a.id=#{id} ")
	@Results({
		@Result(property="id", column="id"),
		@Result(property="name", column="title"),
		@Result(property="author", column="author"),
		@Result(property="thumnailURL", column="thumbnail"),
		@Result(property="date", column="created_date"),
		@Result(property="category.id", column="category_id"),
		@Result(property="category.name", column="category_name"),
	})
	public Article findOne(int id);
	
	@Select("SELECT id,title,author,thumbnail,a.created_date,c.category_id, c.name as category_name "
			+"from tb_articles a INNER JOIN tb_category c"
			+ " ON  a.category_id=c.id WHERE a.id=#{id} ")
	@Results({
		@Result(property="id", column="id"),
		@Result(property="name", column="title"),
		@Result(property="author", column="author"),
		@Result(property="thumnailURL", column="thumbnail"),
		@Result(property="date", column="created_date"),
		@Result(property="category.id", column="category_id"),
		@Result(property="category.name", column="category_name"),
	})
	public List<Article> findAll();
	@Delete("DELETE FROM tb_articles WHERE id=#{id}")
	public void delete(int id);
	
	@Update("UPDATE tb_articles SET title=#{name}, author=#{author} WHERE id=#{id}")
	public void update(Article article);
}
