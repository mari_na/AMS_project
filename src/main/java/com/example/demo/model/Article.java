package com.example.demo.model;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Article {
	@NotNull
	private int id;
	@NotBlank
	private String name;
	@NotBlank
	private String author;
	private String date;
	private String thumnailURL;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getThumnailURL() {
		return thumnailURL;
	}
	public void setThumnailURL(String thumnailURL) {
		this.thumnailURL = thumnailURL;
	}
	public Article(@NotNull int id, @NotBlank String name, @NotBlank String author, String date, String thumnailURL) {
		super();
		this.id = id;
		this.name = name;
		this.author = author;
		this.date = date;
		this.thumnailURL = thumnailURL;
	}
	public Article() {
		super();
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "Article [id=" + id + ", name=" + name + ", author=" + author + ", date=" + date + ", thumnailURL="
				+ thumnailURL + "]";
	}
	
}
