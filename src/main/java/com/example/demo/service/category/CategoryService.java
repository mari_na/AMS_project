package com.example.demo.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.demo.model.Category.Category;
import com.example.demo.repository.category.CategoryRepository;
@Service
public class CategoryService {
	@Autowired
	private CategoryRepository categoryRepo;
	
	@Autowired
	public void setCategoryRepo(CategoryRepository categoryRepo) {
		this.categoryRepo = categoryRepo;
	}
	public Category findOne(int id) {
		// TODO Auto-generated method stub
		return categoryRepo.findOne(id);
	}
	public void add(Category category) {
		// TODO Auto-generated method stub
		categoryRepo.add(category);
	}

	public List<Category> findAll() {
		return categoryRepo.findAll();
	}

	

}
