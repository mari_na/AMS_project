package com.example.demo.configuration;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

@Configuration
public class MultiLanguagesConfiguration extends WebMvcConfigurerAdapter{
	
	@Bean
	public LocaleResolver localeResolver() {
		 SessionLocaleResolver sLocaleResolver = new SessionLocaleResolver();
		 sLocaleResolver.setDefaultLocale(Locale.US);
		 return sLocaleResolver;
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lChangeInterceptor = new LocaleChangeInterceptor();
		lChangeInterceptor.setParamName("lang");
		return lChangeInterceptor;
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// TODO Auto-generated method stub
		registry.addInterceptor(this.localeChangeInterceptor());
	}
}
